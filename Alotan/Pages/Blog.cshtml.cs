﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Alotan.Pages
{
    public class BlogModel : PageModel
    {

        private readonly List<BlogItem> BlogData = new List<BlogItem> {

            new BlogItem
            {
                Url = "images/img_6.jpg",
                Type = "images",
                Description = "Best Places in Australia"
            },
            new BlogItem
            {
                Url = "images/img_2.jpg",
                Type = "ios-videocam",
                Description = "Video in Stockton Beach, Austrlia"
            },
            new BlogItem
            {
                Url = "images/img_1.jpg",
                Type = "iamges",
                Description = "Photography in Trogir, Croatia"
            },
            new BlogItem
            {
                Url = "images/img_3.jpg",
                Type = "document",
                Description = "Enjoyimg the Desert in Morocco"
            },
            new BlogItem
            {
                Url = "images/img_4.jpg",
                Type = "images",
                Description = "Travel in India's Popular Places"
            },
            new BlogItem
            {
                Url = "images/img_5.jpg",
                Type = "document",
                Description = "Popular Tower in France"
            },
            new BlogItem
            {
                Url = "images/img_5.jpg",
                Type = "document",
                Description = "Popular Tower in France"
            }

        };

        private int numPage = 3;

        public PaginatedList<BlogItem> BlogList { get; set; }

        public class BlogItem
        {
            public string Url { get; set; }

            public string Type { get; set; }

            public string Description { get; set; }
        }
        

        [BindProperty(SupportsGet = true)]
        public int? pageNumber { get; set; }
        public void OnGet()
        {
            BlogList = PaginatedList<BlogItem>.Create(BlogData, pageNumber ?? 1, numPage);
        }       

        public class PaginatedList<T> : List<T>
        {
            public int PageIndex { get; private set; }
            public int TotalPages { get; private set; }

            public PaginatedList(List<T> items, int count, int pageIndex, int pageSize)
            {
                PageIndex = pageIndex;
                TotalPages = (int)Math.Ceiling(count / (double)pageSize);
                this.AddRange(items);
            }

            public bool HasPreviousPage
            {
                get
                {
                    return (PageIndex > 1);
                }
            }

            public bool HasNextPage
            {
                get
                {
                    return (PageIndex < TotalPages);
                }
            }

            public static PaginatedList<T> Create(List<T> source, int pageIndex, int pageSize)
            {
                var count = source.Count();
                var items = source.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
                return new PaginatedList<T>(items, count, pageIndex, pageSize);
            }
        }
    }
}