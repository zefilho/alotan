﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using MailKit.Security;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using MimeKit;

namespace Alotan.Pages
{
    public class ContactModel : PageModel
    {
        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [Required]
            [Display(Name = "Name")]
            [MinLength(5)]
            [MaxLength(50)]
            public string Name { get; set; }

            [Required]
            [RegularExpression(@"(\(\d{2}\)\s)(\d{4,5}\-\d{4})")]            
            [Display(Name = "Phone")]
            public string Phone { get; set; }

            [Required]
            [EmailAddress]
            [Display(Name = "E-mail")]
            public string Email { get; set; }

            [Required]
            [Display(Name = "Write a Message")]
            [DataType(DataType.MultilineText)]
            [MinLength(5)]
            [MaxLength(150)]
            public string Message { get; set; }
        }
        public void OnGet()
        {

        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {

                try
                {                    
                    //From Address    
                    string FromAddress = Input.Email;
                    string FromAdressTitle = Input.Name + '|' + Input.Phone;
                    //To Address    
                    string ToAddress = "@email";
                    string ToAdressTitle = "Microsoft ASP.NET Core";
                    string Subject = "Page Contact-Form";
                    string BodyContent = Input.Message;

                    //Smtp Server    
                    string SmtpServer = "smtp.office365.com";
                    //Smtp Port Number    
                    int SmtpPortNumber = 587;

                    var mimeMessage = new MimeMessage();
                    mimeMessage.From.Add(new MailboxAddress
                                            (FromAdressTitle,
                                             FromAddress
                                             ));
                    mimeMessage.To.Add(new MailboxAddress
                                             (ToAdressTitle,
                                             ToAddress
                                             ));
                    mimeMessage.Subject = Subject; //Subject  
                    mimeMessage.Body = new TextPart("plain")
                    {
                        Text = BodyContent
                    };

                    using (var client = new MailKit.Net.Smtp.SmtpClient())
                    {
                        await client.ConnectAsync(SmtpServer, SmtpPortNumber, SecureSocketOptions.StartTls);
                        await client.AuthenticateAsync(
                            "UserName",
                            "Password"
                            );
                        await client.SendAsync(mimeMessage);                        
                        await client.DisconnectAsync(true);
                    }

                    return Redirect("./Index");

                }
                catch (Exception e)
                {
                    Console.WriteLine("E-mail não enviado");                    
                    ViewData["Error"] = $"Erro no enviou da mensagem.";
                }

                
            }

            return Page();
        }
    }
}